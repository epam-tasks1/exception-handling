﻿using System;

namespace ExceptionHandling
{
    public static class ThrowingExceptions
    {
        public static void CheckParameterAndThrowException(object obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj), "obj is null.");
            }
        }

        public static void CheckBothParametersAndThrowException(object obj1, object obj2)
        {
            if (obj1 is null)
            {
                throw new ArgumentNullException(nameof(obj1), "obj1 is null.");
            }

            if (obj2 is null)
            {
                throw new ArgumentNullException(nameof(obj2), "obj2 is null.");
            }
        }

        public static string CheckStringAndThrowException(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                throw new ArgumentNullException(nameof(str), "str is null.");
            }

            return str;
        }

        public static string CheckBothStringsAndThrowException(string str1, string str2)
        {
            if (string.IsNullOrEmpty(str1))
            {
                throw new ArgumentNullException(nameof(str1), "str1 is null.");
            }

            if (string.IsNullOrEmpty(str2))
            {
                throw new ArgumentNullException(nameof(str2), "str2 is null.");
            }

            return str1 + str2;
        }

        public static int CheckEvenNumberAndThrowException(int evenNumber)
        {
            if (evenNumber % 2 != 0)
            {
                throw new ArgumentException($"{evenNumber} is not an even number.");
            }

            return evenNumber;
        }

        public static int CheckCandidateAgeAndThrowException(int candidateAge, bool isCandidateWoman)
        {
            if ((candidateAge < 16 || candidateAge > 63) && !isCandidateWoman)
            {
                throw new ArgumentOutOfRangeException(nameof(candidateAge), "candidateAge is out of range.");
            }

            if ((candidateAge < 16 || candidateAge > 58) && isCandidateWoman)
            {
                throw new ArgumentOutOfRangeException(nameof(candidateAge), "candidateAge is out of range.");
            }

            return candidateAge;
        }

        public static string GenerateUserCode(int day, int month, string username)
        {
            if (day < 1 || day > 31)
            {
                throw new ArgumentOutOfRangeException(nameof(day), "day is out of range.");
            }

            if (month < 1 || month > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(month), "month is out of range.");
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException(nameof(username), "username is null.");
            }

            return $"{username}-{day}{month}";
        }
    }
}
